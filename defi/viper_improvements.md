Problems
--------

1. Crashes while handling parser / type checker errors
   (low)
2. Not type-parametric methods/functions
   (low)
3. No guarded imports
   (medium)
4. Wrong error locations in multi-file Viper programs
4. Generic type checking fails in the following case:
    
    ```silver
    method foo() {
        var x: Map[Ref, Int]
        x := Map()
    }
    ```

5. Cover type parameters in the tutorial

    ```silver
    domain Foo[T, S] {
        function t(x: T): Foo[T, S]
        function s(x: S): Foo[T, S]
    }

    method foo() {
        // var x: Map[Ref, Bool]
        // x := Map()
        // x := Map[Ref, Bool]()

        assume t(0) == s(true)
        assert (t(0):Foo[Int, Bool]) == s(true)
    }
    ```

6. Viper IDE -- caching sometimes crashes


IDE Crashes
-----------

- Type error in the context of `Map`:

    ```silver
    field HashMap_OrderId_repr: Map[OrderId, Order]

    function HashMap_OrderId_footprint(
        self: Ref
    ): Set[Ref]  // error here
        requires acc(self.HashMap_OrderId_repr)
    {
        range(self.HashMap_OrderId_repr)
    }
    ```